### Homeflow Sonar App

## Info

I spent a few hours playing around with different ways of building a homeflow api based web app, primarily looking at react/ rails and angular/rails build as well as some self contained builds. In hindsight I would have just focused on just one build so I could have focused more on the front-end side of things.

First version of this app I built with webpacker and react-rails gem so the whole app could be run from the  sonar-test-api root folder.

I then thought it better to separate the react frontend from the rails api backend as I thought this to be a better working practice for bigger teams and apps (using the the 'react' sonar folder or the front-end and the 'rails' sonar-test-api for the back-end).

Another version of this app involved using angular on the front-end hence the sonarNG folder but I thought for a light rails based SPA, that react was better suited.

I also created a quick little svg logo in Illustrator and used the pre-exisiting animation from the create-react-app install to give it a rotating sonar-ish feel.

The last version of the app I was working on is SonarRailsAPI which was a self contained build - basically, I exposed the API and was going to then work on the front end of the app (using the logo I created) and probably just use bootstrap for basic styling of the app. In hindsight I would have spent a couple of hours on one build rather than playing around with other builds

### Instructions
for sonar, cd into it, install the dependencies and run npm run start (beginning of react-rails build)
for sonar-test-api and sonarRailsAPI cd into the folders, install dependencies and run rails s
for sonarNG, install dependencies run ng serve

## todo
- create .env folder to store api key that i can then hide with the .gitignore file
- Test responsiveness of app, ideally used flexbox and mediaqueries, but if needed look at bulma if a css grid system is needed
- decide on one way of building app and in hindsight spend 2-3 hours on one particaulr build rather than several half builds
