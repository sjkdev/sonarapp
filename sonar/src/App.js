import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Sonar from './sonar/sonar';

const API = '../../sonar-api-test/app/models/Sonar'
const DEFAULT_QUERY = 'redux';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Sonar - Homeflow</h1>

        </header>
        <p className="App-intro">
          <h3 className="App-title">A home away from home</h3>
        </p>
        <Sonar />
      </div>
    );
  }
}

export default App;
