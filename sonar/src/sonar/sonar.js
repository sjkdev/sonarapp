import React from 'react';

class Sonar extends React.Component {
	render(){
		return(
			<ul>
			 	<li>Location: {this.props.sonar.location}</li>
			 	<li>Min Price: {this.props.sonar.min_price}</li>
			 	<li>Max Price: {this.props.sonar.max_price}</li>
			 	<li>No. of rooms: {this.props.sonar.bedrooms}</li>
			 	<li>Sale or Rent: {this.props.sonar.status}</li>
			</ul>
		)
	}
}

export default Sonar;