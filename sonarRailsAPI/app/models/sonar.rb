class Sonar < ApplicationRecord
	include HTTParty
	 # base_uri 'http://index1.homeflow.co.uk/properties/4631183?api_key=77467477edfd2689cd77796a2c4b019f'
  base_uri 'http://index1.homeflow.co.uk/properties?api_key=77467477edfd2689cd77796a2c4b019f&search[place][id]=51e7c4a873dadaf60feee624&[search][channel]=sales'
  # attr_reader :options

  # def initialize
  #   api_key  = Y337e293b514155f364766a2d297931
  #   @options = {
  #     query: {
  #       key: api_key,
  #       sign: "true",
  #       desc: "true",
  #       page: 20
  #     }
  #   }
  # end

  def get_data
    self.class.get('http://index1.homeflow.co.uk/properties?api_key=77467477edfd2689cd77796a2c4b019f&search[place][id]=51e7c4a873dadaf60feee624&[search][channel]=sales')
  end

  def events
    if get_data.code.to_i == 200
      get_data.parsed_response
    else
      raise "Error fetching data from Homeflow API"
    end
  end
end
